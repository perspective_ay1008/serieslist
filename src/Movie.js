import React from 'react';
import styled from 'styled-components';
import { Draggable } from 'react-beautiful-dnd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Container = styled.div`
   margin:8px;
   padding:10px;
   font-style:italic;
   font-size:16px;
   border-bottom: 1px solid black;
   text-align:center;
   display:flex; 
   flex-diraction:row; 
   justify-content: space-between;
`;

export default class Movie extends React.Component{
    render(){
        return(
            <Draggable draggableId={this.props.task.id} index={this.props.index}>
                { (provided,snapshot) => (
                <Container 
                    ref={provided.innerRef}
                    {...provided.draggableProps} 
                    {...provided.dragHandleProps}
                    isDragging ={snapshot.isDragging}
                 >
                       {this.props.task.title}
                       <FontAwesomeIcon icon='trash' onClick={this.props.deleteMovie.bind(this, this.props.task.id,this.props.index)}/> 
                       
                    </Container>
                )}
            </Draggable>
        );
    }
}

 