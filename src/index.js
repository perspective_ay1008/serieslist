import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import Column from './Column';
import InitialData from './InitialData';
import { DragDropContext} from 'react-beautiful-dnd';
import SimpleStorage from 'react-simple-storage';
import FormMovie from './FormMovie';
import { library } from '@fortawesome/fontawesome-svg-core';
import {faTrash} from '@fortawesome/free-solid-svg-icons';

library.add(faTrash);

const Container = styled.div`
display:flex;
flex-direction: row;
flex-grow:1;
justify-content: space-around;
min-height:200px;
`;

class App extends React.Component{
  
  constructor(props){
    super(props);
    this.state=InitialData;
  }
  
  onDragEnd = result => {
    const {destination,source,draggableId} = result;
   
    if (!destination){
      return;
    }
   
    if (destination.droppableId === source.droppableId && destination.index === source.index){
      return;
    }
    
    const start = this.state.columns[source.droppableId];
    const finish = this.state.columns[destination.droppableId];

    if (start === finish){
      const newMIds = Array.from(start.mIds);
      newMIds.splice(source.index,1);
      newMIds.splice(destination.index,0,draggableId);
  
      const newColumn = {
        ...start,
        mIds: newMIds,
      };
  
      const newState = {
         ...this.state,
         columns: {
           ...this.state.columns,
           [newColumn.id]:newColumn,
         },
      };
      
      this.setState(newState);   
      return;
    }
  
    const startMIds = Array.from(start.mIds);
    startMIds.splice(source.index,1);
    const newStart={
      ...start,
      mIds: startMIds,
    };

    const finishMIds= Array.from(finish.mIds);
    finishMIds.splice(destination.index,0,draggableId);
    const newFinish = {
        ...finish,
        mIds: finishMIds,
    };

    const newState ={
      ...this.state,
      columns:{
        ...this.state.columns,
        [newStart.id]:newStart,
        [newFinish.id]:newFinish,
      },
    };
    this.setState(newState);
};

deleteMovie = (colId,key,indx) => {
 // console.log(colId);
  //console.log(key);
  //console.log(indx);
  
  let col =this.state.columns[colId];

  //console.log(col);

  let mids = Array.from(col.mIds);

  //console.log(mids);
  if (mids[indx] === key) mids.splice(indx,1);
  else console.log('error');
  //console.log(mids);

  const newColumn = {
    ...this.state.columns[colId],
    mIds: mids,
  };

   //console.log(newColumn);

  const newState = {
     ...this.state,
     columns: {
       ...this.state.columns,
       [newColumn.id]:newColumn,
     },
  };
  delete newState['tasks'][key];
  //console.log(newState);

  this.setState(newState);
};

  
addMovie = ( item => {
        let nid = Math.floor(Math.random() * (100 - 44) + 43);
        //console.log(item);
        //console.log(nid);
        let task = {
        id: String('t-'+nid),
        title: String(item),
      }

     // console.log(task);

      let col1=this.state.columns['column-1'];
      const newMIds = Array.from(col1.mIds);
      newMIds.push(task.id);

      const newColumn = {
        ...col1,
        mIds: newMIds,
      };

          const newState = {
            tasks:{...this.state.tasks,...{[task.id]:task}},
             columns: {
               ...this.state.columns,
               [newColumn.id]:newColumn,
             },
          };

          //console.log(newState);

          this.setState(newState);
          });   
    

  render(){
      return (
        <DragDropContext onDragEnd={this.onDragEnd}> 
         <FormMovie onSubmit={this.addMovie}/>
          <Container>
          <SimpleStorage parent={this}/>
         {this.state.columnOrder.map(columnId => {
           const column = this.state.columns[columnId];
           const tasks = column.mIds.map(mId => this.state.tasks[mId]);
           return (<Column key={column.id} column={column} tasks={tasks} deleteMovie={this.deleteMovie} />)
         })
         }
         </Container>
       </DragDropContext>
    )
  };
};

ReactDOM.render(<App />, document.getElementById('root'));