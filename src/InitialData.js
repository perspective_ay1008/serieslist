let InitialData = {
    tasks :{
        't-1':{id:'t-1',title:'Black-and-white Love (s)',
        },
        't-2': {id:'t-2', title:'Two of a Kind (m)',
        },
        't-3':{
            id:'t-3',
            title:'Irreplaceable You (m)',
        },
        "t-4":{
            id:"t-4",
            title:"The Help (m)",
        },
        "t-5":{
            id:"t-5",
            title:"Boy Erased (m)",
        },
        "t-6":{
            id:"t-6",
            title:"Spinning Out (s)",
        },
        "t-7":{
            id:"t-7",
            title:"The Pursuit of Happiness (m)",
        },
        "t-8":{
            id:"t-8",
            title:"The Man Inside Me(m)",
        },
        "t-9":{
            id:"t-9",
            title:"When Sun Meets Moon (m)",
        },
        "t-10":{
            id:"t-10",
            title:"Lady Bird (m)",
        } ,
        "t-11":{
            id:"t-11",
            title:"The Legend of 1900 (m)",
        },
        "t-12":{
            id:"t-12",
            title:"Timm Thaler (m)",
        },
        "t-13":{
            id:"t-13",
            title:"Suspiria (m)",
        },
        "t-14":{
            id:"t-14",
            title:"Terius Behind Me (s)",
        },
        "t-15":{
            id:"t-15",
            title:"Shameless (s)",
        },
        "t-16":{
            id:"t-16",
            title:"11.22.63 (s)",
        },
        "t-17":{
            id:"t-17",
            title:"The Order (s)",
        },
        "t-18":{
            id:"t-18",
            title:"The Office (s)",
        },
        "t-19":{
            id:"t-19",
            title:"13 Reasons Why (s)",
        },
        "t-20":{
           id:"t-20",
            title:"Tomorrow I Will Date With Yesterday’s You (m)",
        },
        "t-21":{
            id:"t-21",
            title:"One Tree Hill (s)",    
        },
       "t-22":{
            id:"t-22",
            title:"Don't Tell My Boss (s)",
          },
          "t-23":{
            id:"t-23",
            title:"SKY Castle (s)",
          },
          "t-24": {
            id:"t-24",
            title:"Love & Other Drugs (m)",
          },
          "t-25":{
            id:"t-25",
            title:"How To Be Single (m)",
          },
          "t-26":{
            id:"t-26",
            title:"To All The Boys I've Loved Before (m)",
          },
          "t-27":{
            id:"t-27",
            title:"Someone Great (m)",
          },
          "t-28":{
            id:"t-28",
            title:"Crazy Rich Asians (m)",
          },
          "t-29":{
            id:"t-29",
            title:"Drive Me Crazy (m)",
          },
          "t-30":{
            id:"t-30",
            title:"Full House (s)",
          },
          "t-31":{
            id:"t-31",
            title:"Boy Meets Worls (s)",
          },
          "t-32":{
            id:"t-32",
            title:"Avatar:The Last Airbender (s)",
          },
          "t-33":{
            id:"t-33",
            title:"Love, Simon (m)",
          },
          "t-34":{
            id:"t-34",
            title:"The Best of Me (m)",
          },
          "t-35":{
            id:"t-35",
            title:"Sex Education (s)",
          },
          "t-36":{
            id:"t-36",
            title:"Class of lies (s)",
          },
          "t-37":{
            id:"t-37",
            title:"Romantic Doctor,Teacher Kim (s)",
          },
          "t-38":{
            id:"t-38",
            title:"Weiwei's Beautiful Smile (s)",
          },
          "t-39":{
            id:"t-39",
            title:"Doctors (s)",
          },
          "t-40":{
            id:"t-40",
            title:"Five Children (s)",
          },
          "t-41":{
            id:"t-41",
            title:"Weightlifting Fairy Kim Bok Joo (s)",
          },
          "t-42":{
            id:"t-42",
            title:"Operation Love (s)",
          }
    },
    columns :{
        'column-1':{
           id:'column-1',
           title: 'Want To Watch',
           mIds:['t-1','t-2','t-3','t-4','t-5','t-6','t-7','t-8','t-9','t-10','t-11','t-12','t-13','t-14','t-15','t-16','t-17','t-18','t-19','t-20','t-21',
           't-22','t-23','t-24','t-25','t-26','t-27','t-28','t-29','t-30','t-31','t-32','t-33','t-34','t-35','t-36','t-37','t-38','t-39','t-40','t-41','t-42']
        },
       'column-2':{
        id: 'column-2',
        title: 'Watched',
        mIds:[]
       }
    },
    columnOrder: ['column-1','column-2']
};
export default InitialData;
