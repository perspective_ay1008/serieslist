import React from 'react';
import styled from 'styled-components';
import Movie from './Movie';
import { Droppable } from 'react-beautiful-dnd';
//import FormMovie from './FormMovie';

const Container = styled.div`
width:600px;
   margin:8px;
   display:flex;
   flex-direction:column;
   align-content: center;
   justify-content: center;
`;

const Title = styled.h3`
align-self:center;
display:block;
font-family: "Times New Roman", serif;
text-align: center;
font-size: 35px;
color: #000000;
`;

const MovieList = styled.div`
    width:400px;
    padding:8px;
    flex-grow:1;
    align-self:center;
    min-height:200px;
`;

export default class Column extends React.Component{

    render(){
        return(
            <Container>
                <Title> {this.props.column.title} </Title>
                <Droppable droppableId={this.props.column.id}>
                    {(provided, snapshot)=>(
                        <MovieList
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                        isDraggingOver={snapshot.isDraggingOver} >
                       {
                       this.props.tasks.map( (task,index) => (
                       <Movie key={task.id} task={task} index={index} deleteMovie={this.props.deleteMovie.bind(this,this.props.column.id)}/>
                       )
                       )}
                       {provided.placeholder}
                        </MovieList>
                    )}
                </Droppable>
            </Container>
        )
    }
}

